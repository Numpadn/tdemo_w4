﻿using System.Collections;
using System.Collections.Generic;
//using Unity.MPE;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    public GameObject blood;
    [SerializeField] private Transform player = null;
    [SerializeField] private Transform respawnPoints = null;
    public Transform deathPoint;

    [Header("Audio/Sound Properties")]
    [Space(10)]
    public AudioSource audioSource;
    public AudioClip wizardWhack;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Instantiate(blood, deathPoint);
            player.transform.position = respawnPoints.transform.position;
            Physics.SyncTransforms();
            audioSource.PlayOneShot(wizardWhack);
        }
    }
}
