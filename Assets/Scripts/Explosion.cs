﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public GameObject bomb;
    public float Power = 10.0f;
    public float Radius = 5.0f;
    public float UpForce = 1.0f; // adds an upwards force to the object being moved 



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void fixedUpdate()
    {
        if (bomb == enabled)
        {
            Invoke("Detonate", 5);
        }
    }

    void Detonate()
    {
        Vector3 explotionPosition = bomb.transform.position;
        Collider[] colliders = Physics.OverlapSphere(explotionPosition, Radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null)
            {
               rb.AddExplosionForce(Power, explotionPosition, Radius, UpForce, ForceMode.Impulse); 
            }
        }
    }

}
