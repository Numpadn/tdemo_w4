﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerMovement_1 : MonoBehaviour
{
    Vector3 playerPos;
    public float speed = 2.0f;
    public float moveDelay = 0.5f;
    public bool isMoving;

    void Start()
    {
        playerPos = transform.position;
    }

    void Update()
    {
        moveDelay -= Time.deltaTime;
        PlayerMove();
        //transform.position = Vector3.MoveTowards(transform.position, playerPos, Time.deltaTime * speed);
        transform.DOMove(playerPos, speed); //snap movement 
    
    }

    public void Movement(Vector3 direction)
    {
        isMoving = true;
        playerPos += direction;
        moveDelay = 0.5f;
    }

    public void PlayerMove()
    {
        if(moveDelay <= 0)
        {
            if(Input.GetKeyDown(KeyCode.W) && !isMoving)
            {
                Movement(Vector3.forward);
                Debug.Log("up");
            }
            if(Input.GetKeyDown(KeyCode.A) && !isMoving)
            {
                Movement(Vector3.left);
                Debug.Log("left");

            }
            if(Input.GetKeyDown(KeyCode.S) && !isMoving)
            {
                Movement(Vector3.back);
                Debug.Log("back");

            }
            if(Input.GetKeyDown(KeyCode.D) && !isMoving)
            {
                Movement(Vector3.right);
                Debug.Log("right");
            }
        }
        else
        {
            isMoving = false;
        }
        
    }
}
