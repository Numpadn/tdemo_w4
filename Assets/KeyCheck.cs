﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCheck : MonoBehaviour
{
    public LevelProperties level;
    public AudioSource soundFXSource;
    public AudioClip keyCollect;

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            level.hasKey = true;
            soundFXSource.PlayOneShot(keyCollect);
            gameObject.SetActive(false);
        }
    }
}
