﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Security.Cryptography;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Space(10)]
    [Header("Character movement properties")]
    public float speed;
    public float rotateSpeed;
    public float jumpPower= 10f;
    public float gravity = 15.0F;
    private Vector3 moveDirection = Vector3.zero;
    public Rigidbody bomb;
    public Transform player;
    CharacterController controller;
    public Animator anim;

    //Chargestuff
    private float strengthf = 5.0f;
    private float strengthb = 5.0f;
    private float strengthu = 5.0f;


    [Header("Audio/Sound Properties")]
    [Space(10)]
    public AudioSource audioSource;
    public AudioClip wizardScream;
    public AudioClip wizardWhack;
    public AudioClip wizardJump;
    
    void Start()
    {
        controller = GetComponent<CharacterController>();
        speed = 5f;
    }

    void Update()
    {
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        float curSpeed = speed * Input.GetAxis("Horizontal");
        controller.SimpleMove(forward * curSpeed);
        if (Input.GetAxis("Horizontal") >= 1 || Input.GetAxis("Horizontal") < 0)
        {
            anim.SetBool("IsWalking", true);

        }
        else
        {
            anim.SetBool("IsWalking", false);
        }
        if(Input.GetKeyDown(KeyCode.Space) && (controller.velocity.y >= -1))
        {
            Debug.Log("Jump");
            anim.SetBool("IsWalking", false);
            PlayerJump();
        }
        /*if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            Debug.Log("DropBomb");
            SpawnBomb(); 
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Debug.Log("ThrowForwardBomb");
            ThrowForwardBomb();
        }*/
        //Charge Dash Mechanics
        if (Input.GetKey("d") && (Input.GetKey(KeyCode.Space)))
        {
            this.strengthf += Time.deltaTime * 10;
        }
        //Forward Dash
        if (this.strengthf >= 14)
        {
            moveDirection.x = strengthf;
            moveDirection.y = strengthf * 0.75f;
            Debug.Log("PopRight");
            this.strengthf = 0;
            audioSource.PlayOneShot(wizardScream);
        }
        //Backwards Dash
        if (Input.GetKey("a") && (Input.GetKey(KeyCode.Space)))
        {
            this.strengthb += Time.deltaTime * 10;
        }
        if (this.strengthb >= 14)
        {
            moveDirection.x = -strengthb;
            moveDirection.y = strengthf * 0.75f;
            Debug.Log("PopLeft");
            this.strengthb = 0;
            audioSource.PlayOneShot(wizardScream);
        }
        //Up Dash
        /*if (Input.GetKey("w") && (Input.GetKey(KeyCode.Space)))
        {
            this.strengthu += Time.deltaTime * 10;
        }
        if (this.strengthu >= 14)
        {
            moveDirection.y = strengthu * 1.5f;
            Debug.Log("Pop");
            this.strengthu = 0;
            audioSource.PlayOneShot(wizardScream);
        }*/
        //Applies both postive and negative drag
        if (moveDirection.x > 0)
        {
            moveDirection.x -= gravity * Time.deltaTime;
        }
        if (moveDirection.x < 0)
        {
            moveDirection.x += gravity * Time.deltaTime;
        }
        //Applies Gravity to the controller
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }

    void PlayerJump()
    {
        //stuff that happens when player jumps
        audioSource.PlayOneShot(wizardJump);
        moveDirection.y = jumpPower;
    }

    void PlayerFall()
    {
        //when player falls to death
        audioSource.PlayOneShot(wizardScream);
    }

    void PlayerHit()
    {
        //stuff that happens when player hits something/gets hit
        //maybe some blood particles (blocky red explosion) 
        audioSource.PlayOneShot(wizardWhack);
    }

   /* void SpawnBomb()
    {
        Instantiate(bomb, player.position, player.rotation);
    }

    void ThrowForwardBomb()
    {
        Rigidbody bombThrow;
        bombThrow = Instantiate(bomb, player.position, player.rotation) as Rigidbody;
        bombThrow.AddForce(player.forward * 2000f);
        bombThrow.AddForce(player.up * 1000f);
    }*/

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        switch (hit.gameObject.tag)
        {
            case "JumpPad": // when player comes in contact with an object with the JumpPad tag they jump highe.
                jumpPower = 20f;
                break;
            case "Ground": // once the palyer hits a ground tag jump goes back to normal.
                jumpPower = 10f;
                break;
        }
    }
}
