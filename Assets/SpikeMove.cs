﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeMove : MonoBehaviour
{
    public float speed = 1f;
    float delta = 1f; 
    public Transform spikes;

    void Update() 
    {
     float y = Mathf.PingPong(speed * Time.time, delta);
     Vector3 pos = new Vector3(spikes.localPosition.x, y, spikes.localPosition.z);
     spikes.localPosition = pos;
    }
}
